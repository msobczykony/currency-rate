Budujemy obrazy kontenerów
>docker-compose build

Stawiamy kontenery
>docker-compose up -d

Wchodzimy w kontener SF5
>docker-compose exec php bash

Tworzymy baze
>php bin/console doctrine:database:create

Maping bazy z migracji
>php bin/console doctrine:migrations:migrate

Pobieranie danych historycznych
>php bin/console currency-rate:import

Testy
>docker-compose exec php-fpm application/tools/phpunit.phar --configuration=application/phpunit.xml.dist --testdox --coverage-text

Endpointy: 
- /currencyrate/list [GET]
- /currencyrate/historical [GET] + w query ?code=EUR&page=1&limit=3
