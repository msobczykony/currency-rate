<?php

namespace App\UI\CLI\Command\CurrencyRate;

use App\Domain\CurrencyRate\Entity\CurrencyRate;
use App\Domain\CurrencyRate\Services\CurrencyService;
use App\Domain\CurrencyRate\Specification\UniqueCurrencyRateSpecification;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

class CurrencyRateImportCommand extends Command
{
    private UniqueCurrencyRateSpecification $uniqueCurrencyRateSpecification;
    private CurrencyService $createService;

    public function __construct(UniqueCurrencyRateSpecification $uniqueCurrencyRateSpecification, CurrencyService $createService)
    {
        $this->uniqueCurrencyRateSpecification = $uniqueCurrencyRateSpecification;
        $this->createService = $createService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('currency-rate:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new DateTime();
        $rates = json_decode(file_get_contents('http://api.nbp.pl/api/exchangerates/tables/A/2021-01-02/'.$now->format('Y-m-d').'/?format=json'), true);
        foreach($rates as $day) {
            foreach($day['rates'] as $rate) {
                $id = Uuid::uuid4();

                $currencyRate = CurrencyRate::createUnique(
                    $this->uniqueCurrencyRateSpecification,
                    $id,
                    $rate['currency'],
                    $rate['code'],
                    new DateTime($day["effectiveDate"]),
                    $rate['mid']
                );

                $this->createService->saveCurrencyRate($currencyRate);
            }
        }
        return 0;
    }
}
