<?php

declare(strict_types=1);

namespace App\UI\HTTP\Controller;

use App\Application\Query\QueryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

abstract class BaseController extends AbstractController
{
    protected MessageBusInterface $queryBus;

    public function __construct(MessageBusInterface $messengerBusQuery)
    {
        $this->queryBus = $messengerBusQuery;
    }

    protected function ask(QueryInterface $query)
    {
        $envelope = $this->queryBus->dispatch($query);

        $stamp = $envelope->last(HandledStamp::class);

        return $stamp->getResult();
    }

}
