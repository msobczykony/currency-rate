<?php

declare(strict_types=1);

namespace App\UI\HTTP\Controller\CurrencyRate;

use App\UI\HTTP\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Application\Query\CurrencyRate\ListQuery;


class ListAction extends BaseController
{
    public function __invoke(Request $request): JsonResponse
    {
        $query = new ListQuery();
        $response = $this->ask($query);

        return new JsonResponse($response);
    }
}
