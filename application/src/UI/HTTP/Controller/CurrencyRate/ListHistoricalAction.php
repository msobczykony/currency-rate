<?php

declare(strict_types=1);

namespace App\UI\HTTP\Controller\CurrencyRate;

use App\Application\Query\CurrencyRate\ListHistoricalQuery;
use App\UI\HTTP\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ListHistoricalAction extends BaseController
{
    public function __invoke(Request $request): JsonResponse
    {
        $page = $request->get('page');
        $limit = $request->get('limit');
        $code = $request->get('code');

        $query = new ListHistoricalQuery($code, (int) $page, (int) $limit);
        $response = $this->ask($query);

        return new JsonResponse($response);
    }
}
