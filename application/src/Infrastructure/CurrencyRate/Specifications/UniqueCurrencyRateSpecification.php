<?php

declare(strict_types=1);

namespace App\Infrastructure\CurrencyRate\Specifications;

use App\Domain\CurrencyRate\Specification\UniqueCurrencyRateSpecification as UniqueCurrencyRateSpecificationInterface;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;
use DateTime;

final class UniqueCurrencyRateSpecification implements UniqueCurrencyRateSpecificationInterface
{
    private $repository;

    public function __construct(CurrencyRateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function isUnique(string $code, DateTime $date): bool
    {
        if($this->repository->findOneByCodeAndDate($code, $date)){
            return false;
        }
        return true;
    }


}
