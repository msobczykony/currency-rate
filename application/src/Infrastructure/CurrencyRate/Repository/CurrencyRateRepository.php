<?php

declare(strict_types=1);

namespace App\Infrastructure\CurrencyRate\Repository;

use App\Domain\CurrencyRate\Entity\CurrencyRate;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyRateRepository implements CurrencyRateRepositoryInterface
{
    private EntityManagerInterface $em;

    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(CurrencyRate::class);
    }

    public function getAll(): array
    {
        return $this->repository->createQueryBuilder('currencyRate')
            ->orderBy('currencyRate.id', 'DESC')
            ->groupBy('currencyRate.code')
            ->getQuery()
            ->getResult();
    }

    public function getHistoricalByCode(string $code, int $limit, int $offset): array
    {
        return $this->repository->createQueryBuilder('currencyRate')
            ->where('currencyRate.code LIKE :code')
            ->orderBy('currencyRate.date', 'DESC')
            ->setParameter('code', '%'.$code.'%')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function findOneByCodeAndDate(string $code, DateTime $date) : ?CurrencyRate
    {
        return $this->repository->findOneBy(['code' => $code, 'date' => $date]);
    }

    public function save(CurrencyRate $currencyRate): void
    {
        $this->em->persist($currencyRate);
        $this->em->flush();
    }
}
