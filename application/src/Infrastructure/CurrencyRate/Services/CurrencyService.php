<?php

declare(strict_types=1);

namespace App\Infrastructure\CurrencyRate\Services;

use App\Domain\CurrencyRate\Entity\CurrencyRate;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;
use App\Domain\CurrencyRate\Services\CurrencyService as RegisterServiceInterface;

class CurrencyService implements RegisterServiceInterface
{
    private CurrencyRateRepositoryInterface $currencyRateRepository;

    public function __construct(CurrencyRateRepositoryInterface $currencyRateRepository)
    {
        $this->currencyRateRepository = $currencyRateRepository;
    }

    public function saveCurrencyRate(CurrencyRate $currencyRate): void
    {
        $this->currencyRateRepository->save($currencyRate);
    }
}
