<?php

declare(strict_types=1);

namespace App\Domain\CurrencyRate\Services;

use App\Domain\CurrencyRate\Entity\CurrencyRate;

interface CurrencyService
{
    public function saveCurrencyRate(CurrencyRate $currencyRate): void;
}
