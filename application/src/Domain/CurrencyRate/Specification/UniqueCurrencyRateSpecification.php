<?php

declare(strict_types=1);

namespace App\Domain\CurrencyRate\Specification;

use DateTime;

interface UniqueCurrencyRateSpecification
{
    public function isUnique(string $code, DateTime $dateTime): bool;
}
