<?php

declare(strict_types=1);

namespace App\Domain\CurrencyRate\Repository;

use App\Domain\CurrencyRate\Entity\CurrencyRate;

interface CurrencyRateRepositoryInterface
{
    public function getAll(): array;

    public function getHistoricalByCode(string $code, int $limit, int $offset): array;

    public function save(CurrencyRate $currencyRate): void;
}
