<?php

namespace App\Domain\CurrencyRate\DTO;

use Ramsey\Uuid\UuidInterface;

class CurrencyRateDTO
{
    public string $id;

    public string $currency;

    public string $code;

    public string $date;

    public string $value;

    public function __construct(string $id, string $currency, string $code, string $date, string $value)
    {
        $this->id = $id;
        $this->currency = $currency;
        $this->code = $code;
        $this->date = $date;
        $this->value = $value;
    }
}