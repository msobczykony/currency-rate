<?php

declare(strict_types=1);

namespace App\Domain\CurrencyRate\Entity;

use App\Domain\CurrencyRate\Specification\UniqueCurrencyRateSpecification;
use DateTime;
use Ramsey\Uuid\UuidInterface;

class CurrencyRate
{
    private UuidInterface $id;

    private string $currency;

    private string $code;

    private DateTime $date;

    private string $value;

    public static function createUnique(UniqueCurrencyRateSpecification $uniqueCurrencyRateSpecification, UuidInterface $id, string $currency, string $code, DateTime $date, string $value): ?self
    {
        if ($uniqueCurrencyRateSpecification->isUnique($code, $date)) {
            return new self(
                $id,
                $currency,
                $code,
                $date,
                $value
            );
        }
        return null;
    }

    private function __construct(UuidInterface $id, string $currency, string $code, DateTime $date, string $value)
    {
        $this->id = $id;
        $this->currency = $currency;
        $this->code = $code;
        $this->date = $date;
        $this->value = $value;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getDate(): string
    {
        return $this->date->format('Y-m-d H:i:s');
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
