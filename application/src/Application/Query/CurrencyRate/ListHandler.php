<?php

declare(strict_types=1);

namespace App\Application\Query\CurrencyRate;

use App\Application\Query\QueryHandlerInterface;
use App\Domain\CurrencyRate\DTO\CurrencyRateDTO;
use App\Domain\CurrencyRate\Entity\CurrencyRate;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;

class ListHandler implements QueryHandlerInterface
{
    private CurrencyRateRepositoryInterface $currencyRateRepository;

    public function __construct(CurrencyRateRepositoryInterface $currencyRateRepository)
    {
        $this->currencyRateRepository = $currencyRateRepository;
    }

    public function __invoke(ListQuery $query): array
    {
        $currencyRates = $this->currencyRateRepository->getAll();
        $response = [];
        foreach($currencyRates as $currencyRate) {
            $response[] = new CurrencyRateDTO($currencyRate->getId()->toString(), $currencyRate->getCurrency(), $currencyRate->getCode(), $currencyRate->getDate(), $currencyRate->getValue());
        }

        return $response;
    }
}
