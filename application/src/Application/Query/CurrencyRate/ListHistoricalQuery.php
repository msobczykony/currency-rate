<?php

declare(strict_types=1);

namespace App\Application\Query\CurrencyRate;

use App\Application\Query\QueryInterface;

class ListHistoricalQuery implements QueryInterface
{
    public string $code;

    public int $page;

    public int $limit;

    public function __construct(string $code, int $page, int $limit)
    {
        $this->code = $code;
        $this->page = $page;
        $this->limit = $limit;
    }
}
