<?php

declare(strict_types=1);

namespace App\Application\Query\CurrencyRate;

use App\Application\Query\QueryHandlerInterface;
use App\Domain\CurrencyRate\DTO\CurrencyRateDTO;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;
use App\Infrastructure\Utils\PagerTrait;

class ListHistoricalHandler implements QueryHandlerInterface
{
    use PagerTrait;

    private CurrencyRateRepositoryInterface $currencyRateRepository;

    public function __construct(CurrencyRateRepositoryInterface $currencyRateRepository)
    {
        $this->currencyRateRepository = $currencyRateRepository;
    }

    public function __invoke(ListHistoricalQuery $query): array
    {
        $page = $this->getPage($query->page);
        $limit = $this->getLimit($query->limit);
        $offset = $this->getOffset($page, $limit);

        $currencyRates = $this->currencyRateRepository->getHistoricalByCode($query->code, $limit, $offset);

        $response = [];
        foreach($currencyRates as $currencyRate) {
            $response[] = new CurrencyRateDTO($currencyRate->getId()->toString(), $currencyRate->getCurrency(), $currencyRate->getCode(), $currencyRate->getDate(), $currencyRate->getValue());
        }

        return $response;
    }
}
