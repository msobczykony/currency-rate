<?php

namespace App\Tests\Infrastructure\CurrencyRate\Services;

use App\Domain\CurrencyRate\Entity\CurrencyRate;
use App\Domain\CurrencyRate\Repository\CurrencyRateRepositoryInterface;
use App\Domain\CurrencyRate\Services\CurrencyService as RegisterServiceInterface;
use App\Infrastructure\CurrencyRate\Services\CurrencyService;
use PHPUnit\Framework\TestCase;

class CurrencyServiceTest extends TestCase
{
    private CurrencyRateRepositoryInterface $currencyRateRepository;

    private CurrencyService $currencyService;

    private CurrencyRate $currencyRate;

    protected function setUp(): void
    {
        $this->currencyRate = $this->createMock(CurrencyRate::class);
        $this->currencyRateRepository = $this->createMock(CurrencyRateRepositoryInterface::class);
        $this->currencyService = new CurrencyService($this->currencyRateRepository);
    }

    public function test__construct()
    {
        $this->assertInstanceOf(RegisterServiceInterface::class, $this->currencyService);
    }

    public function testSaveCurrencyRate()
    {
        $this->currencyRateRepository->expects(self::once())->method('save')->with($this->currencyRate);
        $this->currencyService->saveCurrencyRate($this->currencyRate);
    }
}
